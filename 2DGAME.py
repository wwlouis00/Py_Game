import os
import subprocess
import openpyxl
import sys
import datetime
import pygame
import random
screen = pygame.display.set_mode((640,480))
done = False
barW = 60
barH = 30
barY = 400
while not done:
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
    screen.fill((255,0,0))
    mouseX,mouseY = pygame.mouse.get_pos()
    pygame.draw.rect(screen,(0,128,128),[mouseX,mouseY,barW,barH])
    pygame.display.update()
